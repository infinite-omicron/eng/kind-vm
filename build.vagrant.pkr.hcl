locals {
  version = "0.0.1"
}

build {
  name = "kind-virtual-machine"

  sources = [
    "source.vagrant.alpine"
  ]

  provisioner "shell" {
    scripts = [
      "scripts/dependencies.sh",
    ]
    pause_before      = "10s"
    timeout           = "10s"
    execute_command   = "echo 'vagrant' | sudo -S -E sh -c '{{ .Vars }} {{ .Path }}'"
    expect_disconnect = true
  }

  provisioner "shell" {
    inline = [
      "curl -LO https://raw.githubusercontent.com/kubernetes-sigs/kind/v0.14.0/site/static/examples/kind-with-registry.sh",
      "chmod +x ./kind-with-registry.sh",
      "./kind-with-registry.sh"
    ]
    pause_before = "10s"
    timeout      = "10s"
  }

  post-processors {
    post-processor "vagrant-cloud" {
      access_token = "${var.cloud_token}"
      box_tag      = "infinite-omicron/kind"
      version      = "${local.version}"
    }
  }
}

