# kind-virtual-machine

_Kubernetes IN Docker Virtual Machine._

## Compiling

```
packer build .
```

| Environment Variables |
| --------------------- |
| `VAGRANT_CLOUD_TOKEN` |

## License

SPDX-License-Identifier: [Apache-2.0](LICENSE)

